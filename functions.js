// ассоциативный массив значений ингредиентов на складе
var storage = {
	apple: {
  		quantity: 20,
  		name: 'яблоко',
 	},
 
	strawberry : {
	  	quantity: 20,
	  	name : 'клубника',
	},

	apricot : {
	  	quantity: 20,
	  	name: 'абрикос',
	},

	floor : {
	  	quantity: 20, 
	  	name: 'мука',  
	},
	 
	milk : {
	  	quantity: 20,
	  	name: 'молоко',
	},

	eggs : {
	  	quantity : 50,
	  	name : 'яйцо'
	}
}

// список пирогов
var pies = {
	apple: 'Яблочный',
	strawberry: 'Клубничный',
	apricot: 'Абрикосовый'
}

// ассоциативный массив для каждого рецепта
var pieRecipes = {
  	apple : {
		apple: 3,
		floor: 2,
		milk: 1,
		eggs: 3,
	},

	strawberry: {
		strawberry: 5,
		floor: 1,
		milk: 2,
		eggs: 4,
	},
	
	apricot: {
		apricot: 2,
		floor: 3,
		milk: 2,
		eggs: 2,
	}
}

// переменная кол-ва докупки ингредиентов
var ingredientIncrement = 10;

// фукнция позволяющаю испечь пирог
function bakePie(pietype) {
	var recipe = pieRecipes[pieType]; // получение рецепт для того типа пирога, который необходимо испечь
	
	// проверка достаточности ингредиентов на складе для выпечки пирога выбранного типа
	for (var ingredient in recipe) {
		var quantity = recipe[ingredient]; // отображение кол-ва данного ингредиента для испечения данного пирога

		if( storage[ingredient].quantity < quantity ) {
	 		console.log('Недостаточно ингредиента ' + storage[ingredient].name + '. Покупаем ' + ingredientIncrement + ' ед.');
	  		storage[ingredient].quantity += ingredientIncrement;
		}

		storage[ingredient].quantity -= quantity; // вычитание необходимого для данного пирога кол-ва ингредиентов
	}
}

// пироги пекутся в цикле
for (var i = 0; i < 10; i++) {
	for(var pieType in pies) {
		bakePie(pieType);
		
		console.log( pies[pieType] + ' пирог ' + (i + 1) + ' готов!');		
	}
}

console.log('Все пироги испекли!');

// Выполнение пункта 2
var n = 25;
var sum = sumTo(n);
console.log(sum);

function sumTo(n) {
	console.log(n);

  	return n > 1 ? n + sumTo(--n) : n;
}
