    // исходные данные
    var studentsAndPoints = ['Алексей Петров', 0,
                            'Ирина Овчинникова', 60,
                            'Глеб Стукалов', 30,
                            'Антон Павлович', 30,
                            'Виктория Заровская', 30,
                            'Алексей Левенец', 70,
                            'Тимур Вамуш', 30,
                            'Евгений Прочан', 60,
                            'Александр Малов', 0];

// создаю переменную для хранения списка студентов, чтобы потом через неё выводить все необходимые списки

    var classMarks = "Список студентов:";

// переменные для хранения имени лучшего студента и его оценки
    var bestStudentMark = '0'; // для того чтобы она сравнивалась без ошибки
    var bestStudentName


    for (var i = 0; i < studentsAndPoints.length; i += 2) {
      var name = studentsAndPoints[i];
      var mark = studentsAndPoints[i + 1]

  // выполнение Пункта #1
    classMarks += "\nСтудент " + name + ' набрал ' + mark + ' баллов';

  // выполнение Пункта #2
        if( mark > bestStudentMark ) {
          bestStudentMark = mark;
          bestStudentName = name;
        }
      }

// Вывод результата для Пункта 1
    console.log(classMarks);

// Вывод результата для Пункта 2
    if( bestStudentMark > 0 )
      console.log("\n\nСтудент набравший максимальный балл:\nСтудент " + bestStudentName + ' имеет максимальный балл ' + bestStudentMark);

// добавляю новых студентов в массив, выполнение Пункта 3
    studentsAndPoints.push('Николай Фролов', 0, 'Олег Боровой', 0);

// добавляю баллы заданным студентов в массиве, выполнение Пункта 4
    var index;

    index = studentsAndPoints.indexOf('Антон Павлович');
    if( index >= 0 )
      studentsAndPoints[index + 1] += 10;

    index = studentsAndPoints.indexOf('Николай Фролов');
    if( index >= 0 )
      studentsAndPoints[index + 1] += 10;

// поиск и вывод студентов которые не набрали баллов, выполнение Пункта 5
    classMarks = "\nСтуденты не набравшие баллов:";

    index = studentsAndPoints.indexOf(0);
    while( index > 0 ) {
      var name = studentsAndPoints[index - 1];

      classMarks += "\n" + name;

// в этом же цикле выполненяю Пункт #6 удаляем студентов которые не набрали баллов
          studentsAndPoints.splice(index - 1, 2);

          index = studentsAndPoints.indexOf(0);
          }
// вывод студентов не набравших баллов
      console.log(classMarks);
